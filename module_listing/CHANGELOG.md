# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.0] - 2024-06-10
### Added
- Initial release of the Module List module.
