# Module List

Module List is a Drupal module that provides a user interface for listing all added modules along with their status and version information.

## Installation
1. Download the module files and place them in the `modules/contrib/module_listing` directory of your Drupal installation.
2. Enable the Module List module through the Drupal administration interface (`/admin/modules`).

## Usage
Navigate to the module list page at `/admin/reports/module-listing` to view the list of enabled modules along with their status and version information.

## Features

- Lists all custom, contrib, and core modules.
- Displays module status (enabled/disabled).
- Shows the installed version of each module.
- Provides the latest available version for contrib and core modules.

## Configuration

No additional configuration is required. The module provides a report available to users with the `administer site configuration` permission.

## Requirements
- Drupal 8, 9, 10, or 11

## Issues
If you encounter any issues or have suggestions for improvement, please [submit an issue](https://www.drupal.org/project/issues/module_listing) on Drupal.org.

## Maintainers
- Sunny Sharma (infoofsunnysharma@gmail.com)

## Configuration

No additional configuration is required. The module provides a report available to users with the `administer site configuration` permission.
