<?php

namespace Drupal\module_listing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Update\UpdateManagerInterface;

/**
 * Class ModuleListingController.
 *
 * @package Drupal\module_listing\Controller
 */
class ModuleListingController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new ModuleListingController object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Function to list modules.
   *
   * @return array
   *   Render array for module list.
   */
  public function listModules() {
    // Get all the modules data.
    $modules = \Drupal::service('extension.list.module')->getList();

    $rows = [];

    // Count of modules by type.
    $custom_count = 0;
    $contrib_count = 0;
    $core_count = 0;

    // Sort modules by name and status.
    uasort($modules, function($a, $b) {
      $name_a = $a->name ?? '';
      $name_b = $b->name ?? '';
      if ($a->status == $b->status) {
        return strcmp($name_a, $name_b);
      }
      return $a->status ? -1 : 1;
    });


    // Iterate over each module to categorize and display information.
    foreach ($modules as $module_name => $module) {
      $module_type = $this->getModuleType($module_name);
      $version = $this->getModuleVersion($module_name);

      $row = [
        'name' => $module->info['name'] ?? $module_name,
        'machine_name' => $module_name,
        'status' => $module->status ? $this->t('Enabled') : $this->t('Disabled'),
        'version' => $version,
      ];

      if ($module_type == 'custom') {
        $custom_count++;
        $rows['custom'][] = $row;
      }
      elseif ($module_type == 'contrib') {
        $contrib_count++;
        $rows['contrib'][] = $row;
      }
      else {
        $core_count++;
        $rows['core'][] = $row;
      }
    }

    // Table header.
    $header = [
      $this->t('Module'),
      $this->t('Machine Name'),
      $this->t('Status'),
      $this->t('Version'),
    ];

    // Build render array for the table.
    $build = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => [],
      '#attributes' => ['class' => ['module-listing-table']],
      '#attached' => [
        'library' => [
          'module_listing/module_listing',
        ],
      ],
    ];

    // Add module information rows.
    foreach (['custom', 'contrib', 'core'] as $type) {
      if (!empty($rows[$type])) {
        // Add section header row.
        $build['#rows'][] = [
          [
            'data' => $this->t(ucfirst($type) . ' Modules'),
            'colspan' => 4,
            'class' => ['module-type-header'],
            'style' => 'font-weight: bold; color: black;',
          ],
        ];
        foreach ($rows[$type] as $row) {
          // Style rows based on status.
          $status_style = $row['status'] == $this->t('Enabled') ? 'color: green;' : 'color: red;';
          $row['status'] = [
            'data' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => $row['status'],
              '#attributes' => ['style' => $status_style],
            ],
          ];
          $build['#rows'][] = $row;
        }
      }
    }

    // Render the counts separately and return as a render array.
    $counts = [
      '#markup' => $this->t('Custom modules: @custom_count Contrib modules: @contrib_count Core modules: @core_count', [
        '@custom_count' => $custom_count,
        '@contrib_count' => $contrib_count,
        '@core_count' => $core_count,
      ]),
      '#prefix' => '<div class="module-counts">',
      '#suffix' => '</div>',
    ];

    return [
      $counts,
      $build,
    ];
  }

  /**
   * Retrieves the version of a module.
   *
   * @param string $module_name
   *   The name of the module.
   *
   * @return string
   *   The version of the module, or 'Unknown' if not found.
   */
  private function getModuleVersion($module_name) {
    $module_listing = \Drupal::service('extension.list.module')->getList();

    if (isset($module_listing[$module_name])) {
      $module_info = $module_listing[$module_name]->info;
      if (isset($module_info['version'])) {
        return $module_info['version'];
      }
    }

    return $this->t('Unknown');
  }

  /**
   * Retrieves the type of a module.
   *
   * @param string $module_name
   *   The name of the module.
   *
   * @return string
   *   The type of the module (custom, contrib, core, or unknown).
   */
  private function getModuleType($module_name) {
    // Identify the module type based on its path.
    $module_handler = \Drupal::service('module_handler');
    if ($module_handler->moduleExists($module_name)) {
      $path = $module_handler->getModule($module_name)->getPath();
      if (strpos($path, 'modules/custom') !== FALSE) {
        return 'custom';
      }
      elseif (strpos($path, 'modules/contrib') !== FALSE) {
        return 'contrib';
      }
      elseif (strpos($path, 'core') !== FALSE) {
        return 'core';
      }
    }
    return 'unknown';
  }

}
